### Tools and Evasion Techniques employed by Ransomware Operators to bypass EDR/NDR and ultimately Install Ransomware

With 80% of organizations hit by ransomware attacks in 2021 [https://www.forbes.com/sites/edwardsegal/2022/02/03/a-majority-of-surveyed-companies-were-hit-by-ransomware-attack-in-2021-and-paid-ransom-demands/?sh=415f2b96b8c6] or one attack every 11 seconds [https://cybersecurityventures.com/global-ransomware-damage-costs-predicted-to-reach-20-billion-usd-by-2021/], it’s worthwhile to look at the arsenal of tools that facilitate ransomware installation.  In numerous instances, ransomware installation occurs post exploitation, that is, after the attackers have been able to successfully infiltrate the network via spear-phishing (91% of attacks start with spear-phishing [https://blog.knowbe4.com/bid/252429/91-of-cyberattacks-begin-with-spear-phishing-email]) or other means.  This article shines light upon the tools and frameworks that threat actors use to gain foothold on your network, heighten awareness and response time to remediate the ransomware threat.  Finally, we will discuss a few features that Halcyon Anti-Ransomware leverages in the event of full infiltration.

### C2 Frameworks
C2 Frameworks are usually a client-server architecture that allows an infected host to communicate with the attacks server over a network connection and evade detection.Many of these frameworks have malleable profiles, malleable network configurations and malleable endpoints.  Other architectures exist like P2P (Peer-to-Peer) and out of band communication; however, they are not as common.  For a list of the available C2 frameworks available to attackers and red-teams, have a look at [https://www.thec2matrix.com/matrix] and [https://github.com/tcostam/awesome-command-control].  Legitimate use of these frameworks by red teams is common to test the security of a network, however, ransomware gangs and APT threat actors have utilized these tools for the same reason.  In fact, Ransomware groups have funding and sophistication that previously only APT groups had.  Depending  on the threat actor, post exploitation framework tools contain modifications, especially the configuration files and sometimes source, to bypass YARA signatures, Anti-Virus and network anomaly detection.  There are also internally developed RAT’s (remote access trojans) and C2 frameworks that are not public.  Here are some things to be aware of to protect your network against C2 Frameworks.

### Network Encryption Profiling
Halcyon continuously reverse engineers and monitors emerging ransomware samples.  Many ransomware samples do not require network capabilities to perform their function of encrypting the hard drive, network shares and deleting backups.  However, before installing ransomware, C2 frameworks require network capabilities to permit lateral ransomware installation throughout the company’s network.  The most common C2 communication channel is HTTPS which accounts for 46% of malware [https://news.sophos.com/en-us/2021/04/21/nearly-half-of-malware-now-use-tls-to-conceal-communications/] HTTPS is the most popular because it allows traffic to look normal even as traffic routes through proxies, reverse proxies and domain fronting or masquerading techniques that traverse through legitimate websites.

HTTPS connection negotiation can be fingerprinted before being encrypted during the TLS negotiation between a C2 client and a C2 server.  This is possible because each application contains different packages and methods to negotiate encrypted communication.  As an example, when a C2 Framework creates a client payload and attempts to communicate with the POSH C2 server, the client sends an SSL/TLS Client Hello Packet.  This packet is unique to the client based on version, cipher, extensions and elliptic curves information which is then hashed.  These fingerprints do not change when the IP, domains, X509 certificates or ports change, making them a great tool to detect C2 network traffic for both the client and server([https://github.com/salesforce/ja3]) and should be used together to reduce false positives.  Be aware that C2 operators can modify or spoof the TLS code of the C2 Framework.  Here is a list of JA3 fingerprints collected from 25,000,000 PCAP files of malicious activity ([https://sslbl.abuse.ch/ja3-fingerprints/]).  

TODO: insert halcyon push to use ja3/ja3s inside product to flag c2 and ransomware threats on the endpoint.

Some C2 Frameworks and RATs used in ransomware attacks take things a step further and implement obfuscation features used in the Tor project like ScrambleSuit ([https://www.cs.kau.se/philwint/scramblesuit/]), “a polymorphic network protocol used to circumvent censorship” stacked with a RSA layer.  This allows each instance of its communication channel to be unique by varying packet timing and length.


### Jitter
Jitter, or the variance of timing between the backdoor beaconing to the C2 server, is a configuration setting in many C2 frameworks that the attacker typically modifies to decrease their chances of detection.  Jitter adds randomness to the beacon, however that randomness is typically within a set range of time.  That range of time could be a few seconds to as much as a few days.  Statistical analysis like coefficient of variation (COV), which captures the average relative difference, can be used on an endpoint (soon to be a Halcyon feature) or on the network layer if the time block of analysis is large enough.

![constant_beacon](resources/beacon_constant.png)

![random_beacon](resources/beacon_random.png)


Cobalt Strike Configuration:
```java
set jitter "0";
set sleeptime "60000"; # default sleep in ms, 1 minute
set data jitter "100"; 
```

PoshC2 Configuration:
```python
DefaultSleep: "5s"
Jitter: 0.20
```

Powershell Empire Configuration:
```powershell
[Double]
$AgentJitter = 0,
```


### C2 Generated URLs
C2 Frameworks that use HTTP(S) come with a configurable list of URLs that are meant to blend in with normal looking traffic.  Other C2 Frameworks will generate URL’s based on a wordlist or create random lists that the backdoor will cycle through to evade detection.  Code that can generate domain names is called a Domain Generating Algorithm (https://en.wikipedia.org/wiki/Domain_generation_algorithm).  URLs are encrypted as part of TLS/SSL encryption, making it difficult capture this information unless TLS/SSL inspection is used or a capable endpoint agent is installed.  (Halcyon will inspect urls?).


Example default URL’s from PoshC2

```
/adsense/troubleshooter/1631343/
/adServingData/PROD/TMClient/6/8736/
/advanced_search?hl=en-GB&fg=
/async/newtab?ei=
/babel-polyfill/6.3.14/polyfill.min.js=
/bh/sync/aol?rurl=/ups/55972/sync?origin=
/bootstrap/3.1.1/bootstrap.min.js?p=
/branch-locator/search.asp?WT.ac&api=
```

Example default URL’s from Powershell Empire:

```powershell
$Profile = "/admin/get.php,/news.php,/login/process.php|Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko",
```

Metasploit and Cobalt Strike use a common autogenerated URL scheme.  Some URLs it generates are 4 alphanumeric characters with a constant that matches the URL type.

```python
URI_CHECKSUM_INITW      = 92 # Windows
URI_CHECKSUM_INITN      = 92 # Native (same as Windows)
URI_CHECKSUM_INITP      = 80 # Python
URI_CHECKSUM_INITJ      = 88 # Java
URI_CHECKSUM_CONN       = 98 # Existing session
URI_CHECKSUM_INIT_CONN  = 95 # New stageless session`
```


Since HTTPS hides the URL, this information could only be accessible with an endpoint agent like Halcyon Anti-Ransomware or SSL inspection. ([https://isc.sans.edu/forums/diary/Finding+Metasploit+Cobalt+Strike+URLs/27204/])

It is quite common to see 404 NOT FOUND responses from the C2 server, however upon closer inspection, there might be additional encrypted data within that response. [provide example of 404 hidden data]

### TLS/SSL Certificate Values
Certain C2 Frameworks come with self-signed certificates for encrypting network traffic.  Depending on the resources and capabilities of the attacker, they may use self-signed certificates, certificates generated by Let’s Encrypt ([http://letsencrypt.org]) or commercial certificates.  For example, POSHC2 configuration has these values hard-coded:

POSH C2 Default Self-Signed Certificate Values:
```python
Cert_C = "US"
Cert_ST = "Minnesota"
Cert_L = "Minnetonka"
Cert_O = "Pajfds"
Cert_OU = "Jethpro"
Cert_CN = "P18055077"
Cert_SerialNumber = 1000
Cert_NotBefore = 0
Cert_NotAfter = (10 * 365 * 24 * 60 * 60)
```


Cobalt Strike Default Configuration
```java
https-certificate {

	# Option 1: Self-Signed certificate with Java Keystore tool
	# set keystore "/pathtokeystore";
    # set password "password";
    
    # Option 2: Self Signed with vendor specifics
    
    set C   "US";
    set CN  "beacon.cobaltstrike.com";
    set O   "Help/Systems LLC";
    set OU  "Certificate Authority";
    set validity "365";
}
```

Cobalt Strike comes with a default, self-signed SSL certificate with the serial number: `146473198` ([https://www.shodan.io/search?query=ssl.cert.serial%3A146473198]).  As of this writing there are 579 live servers, with the majority in China, running a version of Cobalt Strike with this default certificate.  The expiration date of a certificate used by any potentially suspicious network connection should be investigated if the certificate has a long expiration, expired expiration, expired recently, or issued within the last 24 hours, week or month.  Maliciously used SSL certificates can be found on blacklist sites like [https://sslbl.abuse.ch/ssl-certificates/].  This site also includes the top 10 issuing Certificate Authorities for malicious SSL use, with Let’s Encrypt as the second most popular.

Beacons also have an embedded SSL public key that is generated on the server at the time of install that allows the infected host to securely encrypt the contents.  This double layered encryption approach prevents data snooping in the event that the SSL/TLS connection is inspected.  Every server will have the same certificate and this data can be used to attribute beacons to specific team server infrastructure or even threat actor.


### Conclusion
Even with multiple layers of security in an enterprise, Ransomware and C2 Frameworks are successfully causing massive disruption to business operations on a daily basis.  Halcyon is designed with the idea that layers will fail and is the first solution to incorporate host isolation, to prevent data exfiltration and lateralization. Bolstered by the industry's first ransomware encryption key capture and file recovery baked into our engine, Halcyon mitigates the risk from ransomware and ensures quick recovery if all layers fail.

The Halcyon engine moves beyond simple behavior analysis of a program and analyzes malicious processes in context of the entire environment via industry-first applications of next-generation machine learning. Modern ransomware knows when it's being examined in a virtualized environment and security products. Halcyon has developed an industry-first semi-virtual deception container to trick ransomware into always thinking it’s being analyzed by these tools or is installing on a computer it's not supposed to be on.`
